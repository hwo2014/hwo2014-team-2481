#include "game_logic.h"
#include "protocol.h"
#include <assert.h>

using namespace hwo_protocol;

const static std::string kName = "Edamameson";
const static double kDefaultThrottle = 0.5;
const static double kDefaultAngleTolerance = 5.0;
const static double kPanicAngle = 20.0;
const static double kEpsilon = 0.0001;
const static double kThrottleDelta = 0.1;
const static double kMaxThrottle = 0.8;
const static double kMinThrottle = 0.0;

bool equals(const double& value, const double& compareValue)
{
  return ( fabs(value - compareValue) <= kEpsilon );
}

game_logic::game_logic()
  : m_lastThrottle(kDefaultThrottle)
  , m_lastDelta(kDefaultThrottle)
  , m_angleTolerance(kDefaultAngleTolerance)
  , action_map
    {
      { "join", &game_logic::on_join },
      { "gameInit", &game_logic::on_game_init },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
      { "yourCar", &game_logic::on_your_car },
      { "turboAvailable", &game_logic::on_turbo_available },
      { "spawn", &game_logic::on_spawn }
    }
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  reset_values();
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
  static long currentCall = 0;
  const static int callsToReport = 60;

  using namespace jsoncons;
  double throttleVal = m_lastThrottle;
  double currentAngle = 0.0;
  bool parsedCorrectly = false;
  try {
    for (size_t i = 0; i < data.size(); ++i)
    {
      json elem = data[i];
      json id = elem["id"];
      if(kName == id["name"].as<std::string>())
      {
        currentAngle = elem["angle"].as<double>();
        parsedCorrectly = true;
      }
    }
  }catch(...) {
    std::cout << "carPositions: error parsing. Using default throttle." << std::endl;
  }
  if(parsedCorrectly)
  {
    if( fabs(currentAngle) < (fabs(m_angleTolerance) - kEpsilon))
    {
      if(equals(m_lastThrottle, 0.0))
      {
        throttleVal = kDefaultThrottle;
      }
      else
      {
        throttleVal = m_lastThrottle + kThrottleDelta;
      }
    }
    else
    {
      if(fabs(currentAngle) >= kPanicAngle)
      {
        throttleVal = 0.0;
      }
      else
      {
        double decelerationScale = fabs(currentAngle / m_angleTolerance);
        throttleVal = m_lastThrottle - (kThrottleDelta * decelerationScale);
        std:: cout << "Decelerating: Throttle = " << throttleVal << std::endl;
      }
    }

    if(throttleVal > kMaxThrottle) throttleVal = kMaxThrottle;
    if(throttleVal < kMinThrottle) throttleVal = kMinThrottle;
  }
  m_lastThrottle = throttleVal;

  //if(throttleVal != kMaxThrottle)//(++currentCall % callsToReport) == 0)
  //  std::cout << "Throttle = " << throttleVal << std::endl;
  if(fabs(currentAngle) > m_angleTolerance)
    std::cout << "carPositon: exceeded angle tolerance:" << currentAngle << std::endl;

  return { make_throttle(throttleVal) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::string name = "someone";
  try{ name = data["name"].as<std::string>(); }catch(...) {}
  std::cout << name << " crashed" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
  m_carColor = data["color"].as<std::string>();
  std::cout << "yourCar color: " << m_carColor << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
    std::cout << "Game Init Received. This is going to be a doozy." << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_available(const jsoncons::json& data)
{
    try {
    m_turboDurationMillis = data["turboDurationMilliseconds"].as<double>();
    //m_turboDurationTicks = data["turboDurationTicks"].as<int>();
    m_turboFactor = data["turboFactor"].as<double>();
    std::cout << "Turbo Available: Duration = " << m_turboDurationMillis << "ms, Ticks = " << m_turboDurationTicks << ", Factor = " << m_turboFactor << std::endl;
    } catch(...) {std::cout << "Error in handler on_turbo_available" << std::endl;}
    //return { make_turbo() };
    return { make_ping() };
}

void game_logic::reset_values()
{
  m_turboDurationMillis = 0.0f;
  m_turboDurationTicks = 0.0f;
  m_turboFactor = 0.0f;
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data)
{
    return { make_throttle(0.1) };
}
