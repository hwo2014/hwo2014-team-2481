set PL="cpp"
set BOTKEY="K8doOdOuWG9jFw"
set BOTNAME="Edamameson"

g++ ^
    main.cpp ^
    protocol.cpp ^
    connection.cpp ^
    game_logic.cpp ^
    -std=c++11 -I jsoncons/src/ -l boost_system -l ws2_32 -o Testbot